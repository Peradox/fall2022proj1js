Web Development Project 1 - Showcase Academic Projects 
Section 00002, Fall 2022
Authors:
Pera Nicholls, 2145293
Minh Phu Ngo, 2142062

Teacher: 
Nasreddine Hallam

Using this site, the user can create a table of academic projects with 8 different fields. Each of these 
have different requirements. If a field does not meet its requirements it cannot be added to the table.

Fields:
-project id: Must start with a letter and can contain only letters, digits, $, and _. 
 Must also be 3-10 characters long.

-owner name: Must start with a letter and can only contain letters, digits, and -.
 Must be 3-10 characters.

-title: letters, numbers, _ allowed. Must be 3-25 characters.

-category: should only be one of 4 options, shown in a drop-down menu. In the table, they will show up as
 "practical", "theory", "fund_res" and "empirical". "------" is a blank option and will not work.

-hours/rate: can only be a number with 1 to 3 digits. 

-status: can only be one of 3 options, 

-description: length is 3-65, can use any characters.


